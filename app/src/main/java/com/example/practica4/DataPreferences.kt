package com.example.practica4

import android.content.Context
import android.renderscript.Sampler
import androidx.core.content.contentValuesOf
import androidx.preference.PreferenceManager

private const val PREF_RFC="dataRFC"


object DataPreferences {
    fun getStoredFRC(context: Context): String{
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(PREF_RFC, "" )!!

    }

    fun setStoredRFC(context: Context, query: String){
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(PREF_RFC, query )
            .apply()




    }


}